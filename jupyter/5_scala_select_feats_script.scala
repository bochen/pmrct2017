import org.apache.spark.sql.Row
import org.apache.spark.ml.linalg.Vectors
import org.apache.spark.ml.classification.{BinaryLogisticRegressionSummary, LogisticRegression}
import org.apache.spark.sql.functions.max
import java.io._
import java.nio.file.{Files, Paths, StandardCopyOption}

def write_text(strings: Array[String], fout: String) = {
    // FileWriter
    val file = new File(fout)
    val bw = new BufferedWriter(new FileWriter(file))
    strings.foreach {
      s =>
        bw.write(s)
        bw.write("\n")
    }
    bw.close()
}


val target_label:Double=9
val binary_count=false
val max_iteration=50
val reg_param=0.03
val use_tfidf=true

val data=spark.sqlContext.read.format("libsvm").load("/tmp/pwclibsvm3").cache()
val t=data.select("label").rdd.map{case Row(u:Double) => if (u==target_label) 1 else 0}.sum
println(s"#label $target_label: $t")


val rddData = data.rdd.map {
    case Row(label: Double, features: org.apache.spark.ml.linalg.Vector) =>          
    val standardv=if(use_tfidf){
         val sparseVec = features.toSparse
         val tf = sparseVec
         val nd=features.size.toDouble
         val dft=sparseVec.values.map(u=>if(u>0) 1.0 else 0).sum
         val idft:Double=math.log(nd/dft)+1
         val newValues: Array[Double] = tf.values.map(u=>u*idft)
         val newsum=math.sqrt(newValues.map(u=>u*u).sum)
         val stdnewvalues = newValues.map(u=>u/newsum)
         Vectors.sparse(sparseVec.size, sparseVec.indices, stdnewvalues)      
    }
    else if(binary_count) { 
         val sparseVec = features.toSparse
         val newValues: Array[Double] = sparseVec.values.map(u=>if(u>0) 1.0 else 0) 
         Vectors.sparse(sparseVec.size, sparseVec.indices, newValues)
    }else {
        features
    }
    val newlabel:Double = if (label==target_label) 1 else 0
    Row.fromSeq(Seq(newlabel, standardv))
}

val training = spark.sqlContext.createDataFrame(rddData, data.schema)

val lr = new LogisticRegression().setMaxIter(max_iteration).setRegParam(reg_param).setElasticNetParam(1)

// Fit the model
val lrModel = lr.fit(training)

// Print the coefficients and intercept for logistic regression
println(s"\nCoefficients size: ${lrModel.coefficients.numActives} Intercept: ${lrModel.intercept}")


// Extract the summary from the returned LogisticRegressionModel instance trained in the earlier
// example
val trainingSummary = lrModel.summary

// Obtain the objective per iteration.
val objectiveHistory = trainingSummary.objectiveHistory
println("objectiveHistory:")
objectiveHistory.foreach(loss => println(loss))

// Obtain the metrics useful to judge performance on test data.
// We cast the summary to a BinaryLogisticRegressionSummary since the problem is a
// binary classification problem.
val binarySummary = trainingSummary.asInstanceOf[BinaryLogisticRegressionSummary]

// Obtain the receiver-operating characteristic as a dataframe and areaUnderROC.
val roc = binarySummary.roc
roc.show(200)
println(s"areaUnderROC: ${binarySummary.areaUnderROC}")


val allwords=sc.union(List(1,2,3,4,5).map(k=>sc.textFile("/tmp/pwc_k"+k))).map(_.split("\t")).map(_.apply(0))

val allwords_map=sc.broadcast(allwords.collect.sorted.zipWithIndex.map(u=>(u._1,u._2+1)).map(_.swap).toMap)
println(allwords_map.value.head)

val lst = collection.mutable.ArrayBuffer.empty[String]
lrModel.coefficients.foreachActive {
          case (i, d) =>
            val w=allwords_map.value(i)
            lst.append(f"${i}\t${w}\t$d%.5f")
        }
lst.foreach(println)
write_text(lst.toArray, s"feats_class${target_label.toInt}_i${max_iteration}_r${reg_param}_${binary_count}_${use_tfidf}.txt")
System.exit(0)

