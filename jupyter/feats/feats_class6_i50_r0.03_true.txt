41920	able circumvent target associate resistance	0.09985
58925	active inactive kinase	0.13827
75208	affinity include small reduction cdk4	0.01032
102434	anomalous difference map semet mad	0.24727
102435	anomalous diffraction	0.03098
103844	antibodies	-0.00902
135858	basal like breast	0.01664
149272	binding kinase inhibitory function equivalent	0.01269
153168	block drug	0.05443
159423	brca1 brca2 include joint effect	0.00812
159563	brca1 brca2 uvs	0.09899
161013	brca1 mutate tumor indication outcome	0.03455
163321	brca2 include non dbd variant	0.00792
163759	brca2 uvs	0.07177
165532	breast cancer indicate	0.00292
165533	breast cancer information	0.00301
168742	bulky metastatic disease receive median	0.05090
172619	cancer brca ovarian cancer characteristic	0.96216
182152	carry brca vus delay make	0.00796
188700	cause heretofore uncertain variant	-0.20171
199798	cell limited	-0.18443
205140	cell select stable	0.01982
229686	close vicinity v247 fig g270	0.04433
254255	conformation kinase inactive state helix	0.17990
263878	contain frequent kra mutant allele	0.01128
268475	contributory	-0.10268
284593	cultural geographic group argue benefit	-0.00064
293203	dash line represent	0.06938
305302	deleterious highly variable change likely	0.00364
305438	deleterious mutation assume time loss	0.03249
306063	deleterious v1688del 5062 5064delgtt vus	0.03841
325211	different degree foci formation ligand	0.47692
327819	diffraction quality crystal use macroseeding	0.03098
343744	docking atp site substrate site	0.13825
345492	domain cooh terminal	0.00888
347329	domain lack	0.01269
351697	dose response assay cell express	0.06489
355595	drug resistance introduce kinase domain	0.13825
375591	emission spectra wild type	0.00000
437160	figure mapk	0.39188
447931	fold difference notice probably reflection	0.05805
472309	gatekeeper mutation affect regulatory spine	0.01982
496180	group hydrolysis gtp bridge oxygen	0.02951
512033	heretofore uncertain variant	-0.20171
531355	i122v k125r t131s permissive activity	0.00471
532975	ic50 value mutant indirect measure	0.17990
550476	include dasatinib	0.01982
561743	individual know activate kinase mutation	0.11182
561744	individual know deleterious	0.11182
570394	inhibitor bind unique conformational state	0.24788
571440	inhibitor include 8220 bisindolylmaleimide similar	0.01982
571441	inhibitor include dasatinib	0.01982
587203	intervention target enzyme effective treat	0.15495
587204	intervention terminus	0.15495
587205	intervention terminus breast	0.15495
587206	intervention terminus breast cancer	0.15495
603930	kinase conformation support hypothesis y572c	0.05089
604610	kinase domain lack	0.01269
605896	kinase inhibitor include	0.01982
605897	kinase inhibitor include dasatinib	0.01982
616067	l726 v299l erbb2 s783 mutation	0.01269
619782	lapatinib bind unfavorable erbb2 mutation	0.01269
619840	lapatinib dos 400 000 nmol	0.01605
619841	lapatinib drug	0.01269
619886	lapatinib expect bind competitively atp	0.01269
619910	lapatinib gain use clinical therapeutic	0.01269
633577	like breast	0.00816
635686	likely share	0.03575
639318	line rescue	0.30848
646946	loop active kinase conformation supplementary	0.14881
693277	model brain	0.00653
701246	motif different kinase align residue	0.18976
721175	mutation brca	0.03537
730401	mutation introduce smad4 251 smad2	0.01982
744709	myriad genetic	0.07880
757611	nmol notably crenolanib 100 fold	0.01982
788606	ovarian cancer available normal tumor	0.08634
844568	plot value fraction monomer fraction	0.01659
850974	pose relevant problem genetic counseling	0.01208
850975	pose significant	0.01069
853315	posses distinct non ptp relate	0.00773
853316	posses enzymatic	0.00773
868170	previousfigure	-0.00645
873435	probability damage splice donor site	0.19102
884269	propose following	0.00948
889979	protein level transcriptional activity	0.00000
892916	protein terminal ser465 ser467 homogeneously	0.00814
901081	purified use vitro transcribe translate	0.01145
901082	purified wild	0.01145
901326	purity protein estimate base sds	0.00000
912714	randomly generate series brca1 variant	0.06768
913433	range unit use parameter transcriptional	0.29068
963270	resistance screen consistent recently report	0.17990
996583	score variable recognize associate brca1	0.00803
1011504	serial dilution yeast tag brca1	0.06848
1011505	serial fold	0.06848
1023934	significant wild type variant score	-0.05248
1029051	single wavelength anomalous	0.03098
1029052	single wavelength anomalous diffraction	0.03098
1030801	site involve coordination site mutation	0.20332
1049745	spectrodesigner	0.00000
1055722	stabilize zinc	0.02006
1108761	target therapeutic intervention	0.15495
1108762	target therapeutic intervention terminus	0.15495
1119190	therapeutic intervention target enzyme effective	0.15495
1119191	therapeutic intervention terminus	0.15495
1119192	therapeutic intervention terminus breast	0.15495
1130655	tran brct repeat protein example	0.33812
1130656	tran deleterious	0.47682
1134056	transfast	-0.40373
1159318	type kinase fig	0.24804
1166464	uncertain variant	-0.20171
1166669	unclassified tsc1 tsc2 variant disease	0.12855
1169333	unfold peptide l330h r337c r342p	0.00000
1178647	use parameter transcriptional activity table	0.25237
1186118	valuable target therapeutic intervention	0.15495
1188747	variant braf	0.03975
1192057	variant migrate marginally ahead wild	-0.20171
1196525	vectibix response preferentially observe ras	-0.00098
1207294	wavelength anomalous	0.00631
1207295	wavelength anomalous diffraction	0.03098
