


lazy val root = (project in file(".")).
  settings(
    name := "p2017",
    version := "1.0",
    scalaVersion := "2.11.8",
    test in assembly := {},
    assemblyOption in assembly := (assemblyOption in assembly).value.copy(includeScala = false),
    fork in Test := true,
    parallelExecution in Test := false

  )

libraryDependencies ++= Seq(

  "ch.qos.logback" % "logback-classic" % "1.1.8",
  "com.typesafe.scala-logging" %% "scala-logging" % "3.5.0",


  "com.github.nikita-volkov" % "sext" % "0.2.6",
  "com.github.scopt" % "scopt_2.11" % "3.5.0",

  "org.apache.spark" % "spark-core_2.11" % "2.0.1" % "provided",
  "org.apache.spark" % "spark-graphx_2.11" % "2.0.1" % "provided",
  "org.apache.spark" % "spark-mllib_2.11" % "2.0.1" % "provided",
  "org.apache.spark" % "spark-hive_2.11" % "2.0.1" % "provided",

  "org.apache.hadoop" % "hadoop-mapreduce-client-core" % "2.7.3" % "provided",
  "org.apache.hadoop" % "hadoop-common" % "2.7.3" % "provided",

  "org.slf4j" % "slf4j-simple" % "1.7.21" % "provided",
  "org.slf4j" % "slf4j-api" % "1.7.21" % "provided",

  "com.holdenkarau" % "spark-testing-base_2.11" % "2.0.1_0.6.0" % "provided",
  "org.scalatest" % "scalatest_2.11" % "2.2.2" % "provided",
  "junit" % "junit" % "4.12" % "provided"
)
