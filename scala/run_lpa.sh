#PL=`expr $PL \* 5`
ENABLE_LOG=false
TARGET=target/scala-2.11/p2017-assembly-1.0.jar
PL=1000
MASTER=spark://genomics-ecs1:7077
SPARK_SUBMIT=~/spark/bin/spark-submit
CMD=`cat<<EOF
$SPARK_SUBMIT --master $MASTER --deploy-mode client --driver-memory 55G --driver-cores 5 --executor-memory 18G  --executor-cores 2 --conf spark.default.parallelism=$PL --conf spark.driver.maxResultSize=5g --conf spark.network.timeout=360000 --conf spark.eventLog.enabled=$ENABLE_LOG --conf spark.speculation=true  \
--class p2017.LPA \
 $TARGET
EOF`

echo $CMD

if [ $# -gt 0 ]
  then
     nohup $CMD &
     echo "submitted"
else
     echo "dry-run, not runing"
fi

