package p2017

import java.io._
import java.nio.file.{Files, Paths, StandardCopyOption}

import scala.io.Source
import breeze.linalg.DenseVector

class Graph(val nodes:Map[String,Node], val neighbors:Map[String,Array[(String,Double)]] ) extends Serializable {
    val node_ids = neighbors.map(_._1).toSeq
    println((nodes.size,node_ids.length,neighbors.size))

    def train_once(w1:Double,w2:Double,w3:Double):Double = {
        node_ids.foreach{
         id=>
            val node=nodes(id)
            val this_neighbors=neighbors(id)
            val s=node.s
            val M = w1*s + this_neighbors.map(_._2).sum * w2 + w3 
        }
        0.0
    }

    def train(max_iter:Int, w1:Double,w2:Double,w3:Double, eps:Double=1e-6) = {
        var err:Double = 10000;
        (0 until max_iter).foreach{
            i=>
            println(err)
            if (err > eps){
                err = this.train_once(w1,w2,w3)
            }
        }
    }
}

class Node(val id:String, val label:Int, max_w:Double) extends Serializable {
    require(label<=9)
    val prior=DenseVector(0.17103282143932549, 0.1361035832580548, 0.026799156880457694, 0.20656428786510087, 0.072869617585064744, 0.082806383619391744, 0.28696175850647393, 0.0057211683227943394, 0.011141222523336344)
    def is_train = label>=0 
    def s:Double= if(is_train) max_w else 0
    val prob = if(is_train){
        val p= DenseVector.zeros[Double](9)
        p(label)=1
        p
    } else {
        prior
    }
}