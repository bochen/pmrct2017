package p2017
    
import org.apache.spark.sql.{SQLContext, SparkSession}   
import org.apache.spark.{SparkConf, SparkContext}

import org.graphframes.GraphFrame

object LPA extends App  {

  override def main(args: Array[String]) {
    val APPNAME = "GraphLPA"

 
        val conf = new SparkConf().setAppName(APPNAME)

        
        val spark = SparkSession
          .builder().config(conf)
          .appName(APPNAME)
          .getOrCreate()
        val sc=spark.sparkContext
        
        sc.setCheckpointDir(System.getProperty("java.io.tmpdir"))
        
        val K=30
        val word_counts=sc.textFile("/tmp/stage2_edges_30.txt").map(_.split(",")).map(u=>(u(0),u(1),u(2).toInt))
        val rawedges=word_counts.filter(u=>u._3>300)
        val nodes_map:Array[(String,Long)] =sc.union(Array(rawedges.map(_._1),rawedges.map(_._2)))
                                                .distinct.zipWithIndex.collect 
        
         val bc1 = sc.broadcast(nodes_map.toMap)
         val bc2 = sc.broadcast(nodes_map.map(_.swap).toMap)
        val edges = spark.sqlContext.createDataFrame(rawedges.map {
          case (src, dst,cnt) =>
            (bc1.value(src), bc1.value(dst), 1)
        }).toDF("src", "dst", "cnt")

           val graph = GraphFrame.fromEdges(edges)
           val cc = graph.labelPropagation.maxIter(200).run() 
           
        //val clusters = cc.select("id", "component")
        val clusters = cc.select("id", "label")
        .rdd.map(x => (x(1).asInstanceOf[Long],  x(0).asInstanceOf[Long]))
        .map(u=>(u._1,Array(bc2.value(u._2))))
        .reduceByKey(_ ++ _).map(_._2)

           clusters.map(_.mkString(",")).saveAsTextFile("/tmp/stage2_graphlpa.txt")

          
        spark.stop()
 
  } //main
}
